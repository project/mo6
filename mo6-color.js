// Support for farbtastic for color picking in theme settings.

// Global killswitch: only run if we are in a supported browser.
if (Drupal.jsEnabled) {
  $(document).ready(function () {
    var farb = $.farbtastic("div.colorpicker");
    $("input.color_textfield").each(function () {
      $(this).css("background-color", farb.color);
      farb.linkTo(this);
      $(this).focus(function () {
        farb.linkTo(this);
      });
    });
  });
}

