<?php

/**
 * @file page.tpl.php
 *
 * Displays a single Drupal page.
 * Added support for changing header and footer images by theme settings.
 * More compact html structure.
 */

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head><title><?php print $head_title ?></title>
<?php print $head ?>
<?php print $styles ?>
<?php 

  $hidebreadcrumb = FALSE;
  $alwaysdisplaybreadcrumb = FALSE;
  $settings = theme_get_settings('mo6'); 
  if (isset($settings)) {
    $hidebreadcrumb = $settings['mo6_breadcrumb'];
    $alwaysdisplaybreadcrumb = $settings['mo6_always_breadcrumb'];
    if ($settings['mo6_use_header']) {

?>
<style type="text/css">
#page_wrapper { background: #f8f8f8 url(<?php print $base_path . $settings['mo6_header_path'] ?>) top left repeat-x; }
#footer { background: #fff url(<?php print $base_path . $settings['mo6_header_path'] ?>) bottom left repeat-x; }
</style>
<?php

    }
    if ($settings['mo6_use_colors']) {

?>
<style type="text/css">
html, body { color: <?php print check_plain($settings['mo6_color_text']) ?>; }
#page_wrapper { background-color: <?php print check_plain($settings['mo6_color_base']) ?>; }
a { color: <?php print check_plain($settings['mo6_color_link']) ?>; }
blockquote { border-left: 2px solid <?php print check_plain($settings['mo6_color_link']) ?>; }
ul.pager li.pager-current { color: <?php print check_plain($settings['mo6_color_link']) ?>; }
ul.pager li a:hover { color: <?php print check_plain($settings['mo6_color_link']) ?>; }
.node h1, h2, h3, h4, h5, h6 { color: <?php print check_plain($settings['mo6_color_header']) ?>; }
h2 a { color: <?php print check_plain($settings['mo6_color_linked_node']) ?>; }
</style>
<?php

    }
    if ($settings['mo6_use_sizes']) {

?>
<style type="text/css">
<?php if (is_numeric($settings['mo6_min_height'])) { ?>
#page_wrapper { min-height: <?php print check_plain($settings['mo6_min_height']) ?>px; }
<?php } ?>
<?php

      if (is_numeric($settings['mo6_content_width'])) { 
        $wrapperwidth = $settings['mo6_content_width'] - 170;
        $sideleft = $settings['mo6_content_width'] - 145;
        $footerwidth = $settings['mo6_content_width'] - 20;
        if ($wrapperwidth > 0) {

?>
#page_wrapper { width: <?php print $wrapperwidth ?>px; }
#side { left: <?php print $sideleft ?>px; }
#footer { width: <?php print $footerwidth ?>px; }
<?php 

        }
      }

?>
</style>
<?php

    }
  }

print $scripts;

?>
</head><body><div id="page_wrapper"><h1><?php

if (strlen(trim($title)) > 0) {
  print $title;
}
elseif (($is_front) && ($site_slogan)) {
  print $site_slogan;
}
else {
  print $head_title;
}

?></h1><?php

if (!$hidebreadcrumb) {
  if ($breadcrumb) {
    print $breadcrumb;
  }
  elseif (($alwaysdisplaybreadcrumb) && (!$is_front)) {

?>
<div class="breadcrumb"><?php print l('Home',''); ?></div>
<?php

  }
}

if ($toppage) {
  print $toppage;
}

if ($tabs) {
  ?><div class="tabs"><?php print $tabs ?></div><?php
}

print $messages; 
print $help;

if ($topcontent) {
  print $topcontent;
}

print $content;

?><div id="side"><?php

if ($left) {
  print $left;
}

if ($right) {
  print $right;
}

?></div>
<?php 

if (isset($site_name)) {
  // Don't display the site name on the front page if there isn't a slogan
  // because in this case the site name is already displayed in the
  // location of the slogan.
  if (!(($is_front) && (!$site_slogan))) {
    print '<div id="sitename">'. l($site_name,'') .'</div>';
  }
}
if (isset($primary_links)) {
  print theme('links', $primary_links, array('class' => 'links primary-links'));
}

?>
<div id="footer"><p><?php print $footer_message; ?> - Powered by <a href="http://drupal.org/" rel="nofollow">Drupal</a> CMS and <a href="http://drupal.org/project/mo6" rel="nofollow">MO6 theme</a></p></div></div><?php print $closure ?></body></html>
