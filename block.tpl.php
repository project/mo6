<?php

/**
 * @file block.tpl.php
 *
 * Theme implementation to display a block. 
 * Add clear-block style, deleted content div.
 */

?><div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="block block-<?php print $block->module ?> clear-block">

<?php if ($block->subject): ?>
  <h2><?php print $block->subject ?></h2>
<?php endif;?>

  <?php print $block->content ?>
</div>
