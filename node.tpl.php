<?php

/**
 * @file node.tpl.php
 *
 * Theme implementation to display a node.
 * Re-ordered content elements.
 */

?><div class="node<?php 

if ($sticky) { 
  print ' sticky'; 
} 

if (!$status) { 
  print ' node-unpublished'; 
} 
?> clear-block"><?php print $picture ?><?php 

if ($page == 0) { 
  ?><h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2><?php 
}

print $content;

if ($submitted) {
  ?><p class="submitted"><?php print $submitted ?></p><?php 
}

if (($terms) && ($page != 0)) { 
  print $terms;
}

if ($links) {
  print $links;
}

?></div>
